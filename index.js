console.log('suresh')

// Helps to see the matrix in same order...
const printArray = function(arr){
    for(let i=0; i< arr.length; i++) {
        const subArr = arr[i];
        let str = ""
        for(let j=0; j< subArr.length; j++) {
            str += (subArr[j] > 9) ? subArr[j]+ "  " : subArr[j]+ "   "
        }
        console.log(str)
    }
}

width = 10
height = 10
mines = 10
auto = false;

/**************** ALGORITHM TO CREATE CORE MATRIX DATA *****************/

// Create a matrix with only 0 and 10 which denotes the cells have mine and non-mines.
createMatrix = function(width, height) {
    const arr = []
    const posArr = []
    for(let i=0; i< height; i++) {
        const subArr = []
        for(let j=0; j<width; j++) {
            if (auto) {
                const randomNumber = Math.random()*width
                if (randomNumber > 2) {
                    subArr.push(0)
                } else {
                    subArr.push(10)
                }
            } else {
                subArr.push(0)
                posArr.push({i,j})
            }
        }
        arr.push(subArr);
    }
    if (!auto) {
        for(let i=0; i< mines; i++) {
            const randomValue = Math.floor(Math.random() * posArr.length);
            arr[posArr[randomValue].i][posArr[randomValue].j] = 10;
            posArr.splice(randomValue, 1);
        }
    }
    return arr;
}

// modify the matrix with related mines count with cell details.
modifyMatrix = function(arr) {
    for(let i=0; i< arr.length; i++) {
        const subArr = arr[i];
        for(let j=0; j< subArr.length; j++) {
            if (subArr[j] == 10) {
                updateCell(arr, i, j-1);
                updateCell(arr, i, j+1);
                updateCell(arr, i-1, j);
                updateCell(arr, i+1, j);
                updateCell(arr, i-1, j-1);
                updateCell(arr, i-1, j+1);
                updateCell(arr, i+1, j-1);
                updateCell(arr, i+1, j+1);
                // arr[i][j-1] += 1
                // arr[i][j+1] += 1
                // arr[i-1][j] += 1
                // arr[i+1][j] += 1
                // arr[i-1][j-1] += 1
                // arr[i-1][j+1] += 1
                // arr[i+1][j-1] += 1
                // arr[i+1][j+1] += 1
            }
        }
    }
    return arr;
}

updateCell = function(arr, i, j) {
    if (arr[i] && arr[i][j] >= 0 && arr[i][j] != 10) {
        arr[i][j] += 1
    }
}

// printArray(mainMatrix)

// printArray(modifyMatrix(mainMatrix))



/**************** CONVERT THE CORE MATRIX INTO DOM AND INTERACTIVE EVENTS *****************/

// MainMtrx = [
//     [1, 1, 1, 0, 0],
//     [1, 10, 1, 0, 0],
//     [1, 1, 1, 0, 0],
//     [0, 0, 0, 1, 1],
//     [0, 0, 0, 1, 10] 
// ]

DomMatrix = []

updateDOMArr = function(array) {
    for (let i = 0; i < array.length; i++) {
        const element = array[i];
        const subArr = [];
        for (let j = 0; j < element.length; j++) {
            subArr.push({
                value: element[j],
                status: 'CLOSED',
                position: {x:i, y:j}
            })
        }
        DomMatrix.push(subArr);
    }
}

renderDom = function() {
    mainDoc = document.getElementById('container');
    // mainDoc.style.width = length * 20 + 20 + 'px'
    for (let i = 0; i < DomMatrix.length; i++) {
        const subArr = DomMatrix[i];
        const row = document.createElement('div');
        row.className = "row";
        mainDoc.appendChild(row)
        for (let j = 0; j < subArr.length; j++) {
            const element = subArr[j];
            const ele = document.createElement('div');
            row.appendChild(ele)
            ele.details = element
            ele.className = "cell";
            ele.classList.add('closed');
            ele.innerHTML = (element.value == 10) ? '<i class="fa fa-bomb" aria-hidden="true"></i>' : element.value;
            ele.addEventListener('click', () => {
                elementClicked(ele)
            })
            ele.addEventListener('contextmenu', function(ev) {
                ev.preventDefault();
                elementRightClikced(ele)
                return false;
            });
            element.cell = ele;
        }
    }
}

openCell = function(cell) {
    cell.classList.remove('closed')
    cell.classList.add('open')
    cell.details.status = "OPEN";
    if (cell.details.value == 0) {
        cell.innerHTML = ""
        const neighboursArr = getNeighbours(DomMatrix, cell.details.position);
        for(let i=0; i< neighboursArr.length; i++) {
            openCell(neighboursArr[i].cell)
        }
    }
}

elementRightClikced = function(cell) {
    if (cell.details.status == "CLOSED") {
        cell.details.status = "BOMP-GUESS";
        cell.innerHTML = '<i class="fa fa-dot-circle-o" aria-hidden="true"></i>';
        cell.classList.add('bomp-guess')
    } else if (cell.details.status == "BOMP-GUESS") {
        cell.details.status = "CLOSED";
        cell.innerHTML = (cell.details.value == 10) ? '<i class="fa fa-bomb" aria-hidden="true"></i>' : cell.details.value;
        cell.classList.remove('bomp-guess')
    } 
}

elementClicked = function(cell) {
    if (cell.details.status == "CLOSED") {
        if (cell.details.value >= 10) {

            setTimeout(() => {
                var r = confirm("OOPS... YOU LOST THE GAME, TRY AGAIN");
                if (r == true) {
                    gamestart()
                } 
            }, 10);
        }
        openCell(cell)
    }
    validate();
}


getNeighbours = function(arr, position) {

    const newArr = [];

    const i = position.x;
    const j = position.y;

    const checkValidCell = (arr, x, y) => {
        if( arr[x] && arr[x][y] && arr[x][y].value >= 0 && arr[x][y].status == "CLOSED" ) {
            newArr.push(arr[x][y])
        }
    }

    checkValidCell(arr, i, j-1); 
    checkValidCell(arr, i, j+1);
    checkValidCell(arr, i-1, j);
    checkValidCell(arr, i+1, j);
    checkValidCell(arr, i-1, j-1);
    checkValidCell(arr, i-1, j+1);
    checkValidCell(arr, i+1, j-1);
    checkValidCell(arr, i+1, j+1);

    return newArr;
}

/**************** VALIDATION AND GAME RESTART *****************/

validate = function() {
    for (let i = 0; i < DomMatrix.length; i++) {
        const subArr = DomMatrix[i];
        for (let j = 0; j < subArr.length; j++) {
            const element = subArr[j];
            if (element.status === "CLOSED" && element.value < 10) {
                return;
            }
        }
    }
    setTimeout(() => alert('CONGRATULATIONS!! YOU WON!!! '), 10);
    
}

gamestart = function() {
    width = document.getElementById("widthInput").value
    height = document.getElementById("heightInput").value
    mines = document.getElementById("mineInput").value
    mainDoc = document.getElementById('container');
    while (mainDoc.firstChild) {
        mainDoc.removeChild(mainDoc.lastChild);
    }
    DomMatrix = [];
    mainMatrix = [];
    mainMatrix = createMatrix(width, height)
    modifyMatrix(mainMatrix)
    updateDOMArr(mainMatrix)
    renderDom();
}

onLoad = function() {
    gamestart();
}